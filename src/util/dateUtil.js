import * as moment from 'moment';

export default class DateUtil {

  static format(date) {
    return moment(date).format("MMMM/DD/YYYY HH:mm a");
  }
}
