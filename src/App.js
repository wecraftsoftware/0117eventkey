import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/style.css';
import Checkin from './components/checkin';
import Nav from './components/nav';
import Header from './components/header';


class App extends Component { }

App.prototype.render = function () { return (
  <div>
  	<Nav />
  	<Header />
    <Checkin />
  </div>
)};

export default App;
