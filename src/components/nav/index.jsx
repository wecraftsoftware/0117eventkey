import React, { Component } from 'react';

export default class Nav extends Component { }

Nav.prototype.render = function() { return (

  <nav className="navbar navbar-default navbar-fixed-top">
    <div className="container-fluid">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#testNavbar">
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a className="navbar-brand" href="/">Event Key Test</a>
      </div>
      <div className="collapse navbar-collapse" id="testNavbar">
        <ul className="nav navbar-nav">
          <li className="active"><a href="/">Home</a></li>
          <li><a href="/">Events</a></li>
        </ul>
        <ul className="nav navbar-nav navbar-right">
          <li><a href="/"><span className="glyphicon glyphicon-user"></span> Hello John Doe</a></li>
          <li><a href="/"><span className="glyphicon glyphicon-log-out"></span> Logout</a></li>
        </ul>
      </div>
    </div>
  </nav>

)}
