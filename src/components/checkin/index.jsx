import React from 'react';
import CheckinService from '../../services/checkin';
import Event from '../event';


export default class Checkin extends React.Component {

  constructor(props) {
    super(props);

    this.state = { checkin: [] };
  }

  componentDidMount() {
    CheckinService.getCheckin()
      .then(response => {
        let checkin = [];
        response.forEach(p => checkin.push(p.attributes));
        this.setState({ checkin });
      });
  }
}

Checkin.prototype.render = function () { return (

  <main>
    { this.state.checkin.map((event, index) => (<Event key={ index } event={ event } />)) }
  </main>

)}
