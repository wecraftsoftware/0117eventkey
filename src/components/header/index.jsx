import React, { Component } from 'react';

export default class Header extends Component { }

Header.prototype.render = function () { return (

  <header>
    <div className="jumbotron text-center">
      <figure>
        <img width="80" src="http://itparty.es/wp-content/uploads/2015/11/party-popper1.png" alt="fiesta"/>
      </figure>
      <h1>Past Events</h1>
      <p>These are the events you have attended.</p>
    </div>
  </header>

)}
