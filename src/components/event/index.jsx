import React from 'react';
import DateUtil from '../../util/dateUtil';

export default class Event extends React.Component { }

Event.prototype.render = function () { return (

  <article>
    <div className="card-container">
      <div className="card-item">
        <div className="row">
          <div className="col-sm-3">
            <figure>
              <img className="img-responsive" alt={ this.props.event.name } src={ this.props.event.image._url }/>
            </figure>
          </div>
          <div className="col-sm-9 flex">
            <h1> { this.props.event.name }</h1>
            <ul>
              <li><span className="glyphicon glyphicon-calendar"></span> { DateUtil.format(this.props.event.startDate) }</li>
              <li><span className="glyphicon glyphicon-user"></span> Number of Attendees: { this.props.event.attendeeCount } </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </article>

)}
