import { PARSE_USER_ID } from '../config/credentials';
import Parse from '../config/parse';

export default class CheckinService {

  static getCheckin() {
    return Parse.Cloud.run('getCheckIn', { userId: PARSE_USER_ID });
  }
}
