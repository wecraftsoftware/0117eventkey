import { PARSE_APP_ID, PARSE_SERVER_URL } from './credentials';
import Parse from 'parse';

Parse.initialize(PARSE_APP_ID);
Parse.serverURL = PARSE_SERVER_URL;

export default Parse;
